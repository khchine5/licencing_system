import unittest

from entities import Customer,Plan,Website

class TestStringMethods(unittest.TestCase):

    def setUp(self):
        self.Single = Plan("Single", 49, 1)
        self.Plus = Plan("Plus", 99, 3)
        self.Infinite = Plan("Infinite", 249, -1)

    def test_customer_entitie(self):
        robin = Customer("Robin", "MyCoolPasword", "robin@gmail.com")
        self.assertEqual(robin.name, 'Robin')
        self.assertEqual(robin.password, 'MyCoolPasword')
        self.assertEqual(robin.email, 'robin@gmail.com')

    def test_plan_entitie(self):
        Single = Plan("Single", 49, 1)
        self.assertEqual(Single.name, 'Single')
        self.assertEqual(Single.price, 49)
        self.assertEqual(Single.number_websites, 1)


    def test_website_entitie(self):
        robin = Customer("Robin", "MyCoolPasword", "robin@gmail.com")
        sub1 = Website("sub1.mydomain.com", robin)
        self.assertEqual(sub1.website_url, "sub1.mydomain.com")
        self.assertEqual(sub1.customer, robin)

    def test_order_website(self):
        robin = Customer("Robin", "MyCoolPasword", "robin@gmail.com")
        robin.subscribe_to_plan(self.Single)
        self.assertEqual(robin.count_ordered_websites, 0)
        self.assertEqual(robin.subscription, self.Single)
        robin.add_website("sub1.domain.com")
        self.assertEqual(robin.count_ordered_websites, 1)
        robin.update_website("sub1.domain.com","sub2.domain.com")
        robin.remove_website("sub2.domain.com")
        


if __name__ == '__main__':
    unittest.main()