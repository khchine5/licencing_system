This project has been created as test for my application.

The project contains 3 files:
- entities.py
- mysaas_test.py
- views.py : This contain my corrections and comments about this file
https://gist.github.com/jbma/3b7e26c595f2e4c05525b0d70f4b3605

The project requires python3.

To run the tests:
python mysaas_test.py