from django.contrib.auth.models import User # This is used on line 11, 19 and 23
from dateutil.relativedelta import relativedelta
from rest_framework import viewsets, status #status is missing
from rest_framework.response import Response
from myapp.serializers import UserSerializer #This is used on on line 10
import json


class UsersViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def create(self, request): #correct indent
        if not request.user.is_authenticated():
            #response was not formatted correctly, added in 401 Unauthorized status
            return Response({"You should be authenticated"}, status=status.HTTP_401_UNAUTHORIZED)
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            comment = request.POST["comment"]
            user = User.objects.create_user( #updated to create_user vs create
                email=serializer.validated_data['email'],
                password=serializer.validated_data['password'],
                comment=comment) #trailing comma and missng closing parentheses 
            user.save() #new user needs to be saved
            response = {'code': 201, 'success': True, 'id': user.id}
            #defaults to 200 status, update for 201 instead
            return Response(response, status=status.HTTP_201_CREATED)
        #removed else block as raise_exception will return a 400 Bad Request automatically