from datetime import datetime
from dateutil.relativedelta import relativedelta

WEBSITES = []
CUSTOMERS = []
PLANS = []

class Customer(object):
    """
    A Customer of our SAAS subscription application
    """

    name = ""
    password = ""
    email = ""
    subscription = None
    subscription_renewal_date = None
    subscription_renewal_date = None
    count_ordered_websites = 0
    paid_amount = 0

    def __init__(self, name, password, email):
        self.name = name
        self.password = password
        self.email = email
        self.count_ordered_websites = 0
        CUSTOMERS.append(self)

    def __str__(self):
        return self.name

    def renew_subscription(self):
        if datetime.today() >= self.subscription_renewal_date:
            print("Renewing your subscription ...")
            self.subscription_renewal_date += relativedelta(years=1)

    def state(self):
        print("John's plan is {0}, has {1} website and his renewel date is {2}".format(
            self.subscription, self.count_ordered_websites, self.subscription_renewal_date))

    def subscribe_to_plan(self, plan):
        self.subscription = plan
        self.subscription_renewal_date = datetime.today() + relativedelta(years=1)
        self.subscription_date = datetime.today()
        self.paid_amount += plan.price

    def change_to_plan(self, plan):
        self.subscription = plan

    def check_subcription(self):
        if not self.subscription:
            print ("You can't add a new website, you need first subscribe to a plan")
            return False
        else:
            return True

    def add_website(self, website_url):
        if not self.check_subcription():
            return False
        if self.subscription.number_websites == -1 or (self.count_ordered_websites < self.subscription.number_websites):
            # Check wether the website url ordred already.
            for website in WEBSITES:
                if website_url == website.website_url:
                    print ("The website {0} is already ordered by {1}".format(website.website_url,website.customer))
                    return False
            new_website = Website(website_url=website_url, customer=self)
            self.count_ordered_websites += 1
            print("The website {0} has been added".format(new_website))
        else:
            print ("You are not allowed to add a new website, you need first subscribe to an other plan")
            return False
    def update_website(self, old_website_url, new_website_url):
        if not self.check_subcription():
            return False
        for website in WEBSITES:
            if website.website_url == old_website_url and website.customer == self:
                website.website_url = new_website_url
                print("Website update to {}".format(new_website_url))
                return True
            print("No website found with url {}".format(old_website_url))
            return False
            
    def remove_website(self,website_url):
        if not self.check_subcription():
            return False
        for website in WEBSITES:
            if website.website_url == website_url and website.customer == self:
                print("Website with url {} has been removed".format(website_url))
                self.count_ordered_websites -= 1
                WEBSITES.remove(website)
                return True
            print("No website found with url {}".format(website_url))
            return False


class Plan(object):
    """
    Avaibles plan of websites
    if number_websites equal to -1 means it is  unlimited number.
    """

    name = ""
    price = 0
    number_websites = 0

    def __init__(self, name, price, number_websites):
        self.name = name
        self.price = price
        self.number_websites = number_websites
        PLANS.append(self)

    def __str__(self):
        return self.name


class Website(object):
    """
    A Website attached to a customer
    """

    website_url = ""
    customer = ""

    def __init__(self, website_url, customer):
        self.website_url = website_url
        self.customer = customer
        WEBSITES.append(self)

    def __str__(self):
        return self.website_url


def simulate():
    # Initiate our three plans
    Single = Plan("Single", 49, 1)
    Plus = Plan("Plus", 99, 3)
    Infinite = Plan("Infinite", 249, -1)

    # Create a customer

    robin = Customer("Robin", "MyCoolPasword", "robin@gmail.com")
    john = Customer("john", "MyCoolPasword2", "john@gmail.com")

    john.subscribe_to_plan(Single)
    john.add_website("myfirst.awesomewebsite.com")
    john.update_website("myfirst.awesomewebsite.com","myfirst11.awesomewebsite.com")
    john.remove_website("myfirst.awesomewebsite.com")

    robin.subscribe_to_plan(Single)
    robin.add_website("myfirst.awesomewebsite.com")
    john.state()

    robin.add_website("myfirst.awesomewebsite.com")
    john.change_to_plan(Plus)
    john.state()

    john.add_website("mysecond.awesomewebsite.com")
    john.state()


if __name__ == '__main__':
    simulate()